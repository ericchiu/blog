
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>+1 購物</title>
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery-3.1.0.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/datepicker-zh-TW.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div>nav</div>
          <div>  <div class="page-header">
  <h1>
    註冊
      </h1>
</div>
</div>
          <div>  <ol class="list-inline lead step-list">
      <li class="step-list__item step-list__item--active">
      <span class="step-list__item__number">&#10122;</span> <span class="step-list__item__title">Facebook 登入授權</span>
    </li>
      <li class="step-list__item step-list__item--">
      <span class="step-list__item__number">&#10123;</span> <span class="step-list__item__title">綁定社團/粉絲專頁</span>
    </li>
      <li class="step-list__item step-list__item--">
      <span class="step-list__item__number">&#10124;</span> <span class="step-list__item__title">完成</span>
    </li>
  </ol>


    <div class="row">
      <div class="col-sm-6">
        <div>
          <div class="form-group">

          </div>
            <form method="get" action="/page2">
                <button type="button" id="fb-login-button" class="btn btn-primary"><i class="fa fa-facebook-official fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;以 Facebook 帳號登入</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="/js/fb_login.js"></script>

  </div>

        </div>
      </div>
    </div>
  </body>
</html>
